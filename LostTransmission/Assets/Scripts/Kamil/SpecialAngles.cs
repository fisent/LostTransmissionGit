﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PlayerAngle
{
    public float angle;
    public float min, max;
    public int directionAnimationIndex;

    public bool Check(float angle)
    {
        return angle >= min && angle <= max;
    }

    private PlayerAngle[] Generate()
    {
        var angles = new PlayerAngle[9];
        for (int i = 0; i < 9; i++)
        {
            float k = (float)i * 45f;
            angles[i] = new PlayerAngle()
            {
                angle = k,
                min = k - 22.5f,
                max = k + 22.5f
            };
        }
        return angles;
    }
}