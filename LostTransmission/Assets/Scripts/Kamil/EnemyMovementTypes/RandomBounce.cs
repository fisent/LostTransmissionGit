﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBounce : AMovementType {

    private float? timeToRotate = null;

    public override AMovementType OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Environment")
        {
            target.SetMovement(Vector3.zero);
            timeToRotate = Random.Range(0f, 2f);
        }
        return this;
    }

    public override AMovementType Update()
    {
        if (timeToRotate != null)
            timeToRotate -= Time.deltaTime;
        var distance = Vector3.Distance(position, PlayerMovement.playerPosition);
        var hits = Physics2D.LinecastAll(position, PlayerMovement.playerPosition, Masks.instance._enviroMask);
        if (hits.Length > 0 || distance > 8)
        {
            if (timeToRotate != null && timeToRotate.Value <= 0)
                RotateRandom();
            return this;
        }
        else
        {
            return new StraightToPlayer() { target = this.target };
        }
    }

    public override AMovementType OnEachSecond()
    {
        if (Random.value < .1f)
            RotateRandom();
        return this;
    }

    protected override void RotateRandom()
    {
        base.RotateRandom();
        timeToRotate = null;
    }
}
