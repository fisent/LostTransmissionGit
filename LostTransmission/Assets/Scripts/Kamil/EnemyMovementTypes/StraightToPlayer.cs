﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightToPlayer : AMovementType {

    public override AMovementType Update()
    {
        var hits = Physics2D.LinecastAll(position, PlayerMovement.playerPosition, Masks.instance._enviroMask);
        var distance = Vector3.Distance(position, PlayerMovement.playerPosition);
        if (hits.Length > 0 || distance > 8)
        {
            return new RandomBounce() { target = this.target };
        }
        else
        {
            target.SetMovement(PlayerMovement.playerPosition - position);
            return this;
        }
    }
}
