﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour {

    [SerializeField]
    private AnimationCurve shakeCurve;
    [SerializeField]
    private float maxStrength;

    private float strength;

    private void Update()
    {
        if(strength <= 0)
        {
            transform.localPosition = Vector3.zero;
        }
        else
        {
            var amplitude = shakeCurve.Evaluate(strength / maxStrength);
            var angle = Random.Range(0f, 360f);
            transform.localPosition = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0f) * amplitude;
            strength = Mathf.Max(0f, strength - Time.deltaTime);
        }
    }

    public void Shake(float str)
    {
        strength = Mathf.Clamp(strength + str, 0f, maxStrength);
    }
}
