﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public static List<EnemySpawner> spawners = new List<EnemySpawner>();
    public static int enemiesCount = 0;

    public int initEnemies;
    public int enemiesAmount;
    public int allowedSpawnsInSec;
    public float distanceFromPlayer;
    public GameObject[] enemyPrefabs;

    IEnumerator Start()
    {
        enemiesCount = 0;
        spawners.Clear();
        spawners.AddRange(FindObjectsOfType<EnemySpawner>());
        for (int i = 0; i < initEnemies; i++)
            Spawn();
        while (true)
        {
            yield return new WaitForSeconds(1f);
            for (int i = 0; i < allowedSpawnsInSec && enemiesCount < enemiesAmount; i++)
                Spawn();
        }
    }

    private void Spawn()
    {
        enemiesCount++;
        var allowedSpawners = spawners.Where(s => Vector2.Distance(s.transform.position, PlayerMovement.playerPosition) >= distanceFromPlayer).ToArray();
        var spawn = allowedSpawners[Random.Range(0, allowedSpawners.Length)];
        Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)], spawn.transform.position, Quaternion.identity).GetComponent<HealthSystem>().onDeath.AddListener(() => { enemiesCount--; });
    }
}
