﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextWriter : MonoBehaviour {

    public Text mesh;
    public string text;

    public float interval, deltaRangeInterval, stopLength;

    IEnumerator Start()
    {
        text = text.Replace("$", "\n");
        mesh.text = "";
        for(int i = 0; i < text.Length; i++)
        {
            while (i < text.Length && text[i] == '#')
            {
                i++;
                yield return new WaitForSecondsRealtime(stopLength);
            }
            mesh.text += text[i];
            yield return new WaitForSecondsRealtime(interval + Random.Range(-deltaRangeInterval, deltaRangeInterval));
        }
    }
}
