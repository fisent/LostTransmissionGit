﻿using UnityEngine;
using UnityEngine.Events;

public class BasicGun : MonoBehaviour
{
    [SerializeField]
    private float shootInterval, shootPointOffset, resetSpreedTime;
    [SerializeField]
    private int bulletAmount;
    [SerializeField]
    private AnimationCurve spreed;
    [SerializeField]
    private GameObject bulletPrefab;

    [SerializeField]
    private KeyCode shootKey;
    [SerializeField]
    private bool repeat;

    private float lastShoot = -100f;
    private float shootIntensity = 0f;

    [SerializeField]
    private UnityEvent onShoot;

    [SerializeField]
    private AudioSourcesWheel audio;
    [SerializeField]
    private AudioClip[] sounds;

    [SerializeField]
    private bool useKeyInput;

    private void Update()
    {
        if (!useKeyInput)
            return;
        if (repeat ? Input.GetKey(shootKey) : Input.GetKeyDown(shootKey))
        {
            Shoot();
        }
    }

    public void Shoot()
    {
        var timeFromLastShoot = Time.time - lastShoot;
        if (timeFromLastShoot >= shootInterval)
        {
            if (timeFromLastShoot >= resetSpreedTime)
                shootIntensity = 0f;
            for (int i = 0; i < bulletAmount; i++)
            {
                var bullet = Instantiate(bulletPrefab, transform.position + transform.up * shootPointOffset, transform.rotation);
                var euler = bullet.transform.eulerAngles;
                var spreadRange = spreed.Evaluate(shootIntensity);
                euler.z += Random.Range(-spreadRange, spreadRange);
                bullet.transform.eulerAngles = euler;
            }
            lastShoot = Time.time;
            shootIntensity++;
            onShoot.Invoke();
            if (sounds.Length > 0)
                audio.GetAudioSource().PlayOneShot(sounds[Random.Range(0, sounds.Length)]);
        }
    }
}
