﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerMovement : Movement
{
    public static Vector3 playerPosition;

    public PlayerAngle[] angles;

    public Animator anim;

    private void Update()
    {
        playerPosition = transform.position;

        motion = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var mouseDirection = mousePosition - transform.position;
        var angle = Vector2.SignedAngle(Vector2.up, mouseDirection);
        var sa = NormalizeAngle(angle);
        head.eulerAngles = new Vector3(0f, 0f, sa.angle);
        var motionAng = NormalizeAngle(Vector2.SignedAngle(Vector2.up, motion));
        if (motion == Vector2.zero)
        {
            anim.SetInteger("direction", -1);
            anim.SetFloat("speed", 0f);
        }
        else
        {
            anim.SetInteger("direction", motionAng.directionAnimationIndex);
            anim.SetFloat("speed", 1f);
        }
    }

    private PlayerAngle NormalizeAngle(float angle)
    {
        while (angle < 0f)
            angle += 360f;
        return angles.First(a => a.Check(angle));
    }
}
