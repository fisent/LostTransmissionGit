﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class HealthBar : MonoBehaviour {

    public float hpStart, hpStep;
    public int hpIntValue;

    public RectTransform hpRect;

    private void Update()
    {
        var v = hpRect.sizeDelta;
        v.y = hpStart + hpStep * hpIntValue;
        hpRect.sizeDelta = v;
    }

    public void SetHp(HealthSystem hp)
    {
        hpIntValue = hp.hp * 10 / hp.maxHp;
    }
}
