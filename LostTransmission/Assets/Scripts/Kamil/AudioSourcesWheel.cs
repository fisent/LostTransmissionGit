﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourcesWheel : MonoBehaviour
{
    [SerializeField]
    private bool pitchInRange;
    [SerializeField]
    private float pitchMin, pitchMax;

    private AudioSource[] sources;

    private int index = 0;
    private int nextIndex
    {
        get
        {
            var i = index;
            index = (index + 1) % sources.Length;
            return i;
        }
    }

    private void Awake()
    {
        sources = GetComponents<AudioSource>();
    }

    public AudioSource GetAudioSource()
    {
        AudioSource source;
        var startIndex = index;
        do
        {
            source = sources[nextIndex];
        }
        while (source.isPlaying && startIndex != index);
        if(pitchInRange)
            source.pitch = Random.Range(pitchMin, pitchMax);
        return source;
    }
}
