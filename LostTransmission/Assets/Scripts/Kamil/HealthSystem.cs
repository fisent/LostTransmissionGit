﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthSystem : MonoBehaviour {

    [SerializeField]
    public int hp;
    [SerializeField]
    public int maxHp;

    [SerializeField]
    private UnityEvent onHurt;
    [SerializeField]
    public UnityEvent onDeath;

    public void Affect(int value)
    {
        if (!enabled)
            return;
        hp = Mathf.Clamp(hp + value, 0, maxHp);
        if (hp == 0)
        {
            onDeath.Invoke();
            enabled = false;
        }
        else if (value < 0)
            onHurt.Invoke();
    }
}
