﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : Movement {

    private AMovementType movementType;

    protected override void Awake()
    {
        base.Awake();
        movementType = new RandomBounce() { target = this };
        var angle = Random.Range(0f, 360f);
        SetMovement(new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)));
        StartCoroutine(OnEachSecond());
    }

    public void SetMovement(Vector2 direction)
    {
        motion = direction;
    }

    IEnumerator OnEachSecond()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            movementType = movementType.OnEachSecond();
        }
    }

    private void Update()
    {
        movementType = movementType.Update();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        movementType = movementType.OnCollisionEnter2D(collision);
    }
}

public abstract class AMovementType
{
    public EnemyMovement target;

    public Vector3 position
    {
        get { return target.transform.position; }
    }

    public virtual AMovementType Update() { return this; }
    public virtual AMovementType OnEachSecond() { return this; }
    public virtual AMovementType OnCollisionEnter2D(Collision2D collision) { return this; }

    protected virtual void RotateRandom()
    {
        var angle = Random.Range(0f, 360f);
        target.SetMovement(new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)));
    }
}