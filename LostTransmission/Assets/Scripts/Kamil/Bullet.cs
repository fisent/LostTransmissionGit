﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour {

    public int dmg;
    public float speed;

    private void Start()
    {
        GetComponent<Rigidbody2D>().velocity = transform.up * speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var healthSystem = collision.gameObject.GetComponent<HealthSystem>();
        if(healthSystem != null)
        {
            healthSystem.Affect(-dmg);
        }
        StartCoroutine(OnHit());
    }

    protected virtual IEnumerator OnHit()
    {
        Destroy(gameObject);
        yield return null;
    }
}
