﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioQueue : MonoBehaviour {

    public AudioSource source;
    public AudioClip[] clips;
    public float delay;

    IEnumerator Start()
    {
        for(int i = 0; i < clips.Length; i++)
        {
            yield return new WaitForSeconds(delay);
            source.PlayOneShot(clips[i]);
            yield return new WaitWhile(() => source.isPlaying);
        }
    }
}
