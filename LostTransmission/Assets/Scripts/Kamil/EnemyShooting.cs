﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour {

    public Sprite[] alive, dead;
    public BasicGun basicGun;
    public Transform head;
    public float shootingDistance;
    public GameObject skull;

    int index;

    private void OnDisable()
    {
        StopAllCoroutines();
    }



    IEnumerator Start()
    {
        index = Random.Range(0, alive.Length);
        GetComponent<SpriteRenderer>().sprite = alive[index];
        while (true)
        {
            yield return new WaitUntil(CanSeePlayer);
            yield return new WaitForSeconds(Random.Range(.1f, 1.5f));
            head.rotation = Quaternion.LookRotation(Vector3.forward, PlayerMovement.playerPosition - head.position);
            basicGun.Shoot();
        }
    }

    private bool CanSeePlayer()
    {
        var distance = Vector2.Distance(transform.position, PlayerMovement.playerPosition);
        var hits = Physics2D.LinecastAll(transform.position, PlayerMovement.playerPosition, Masks.instance._enviroMask);
        return hits.Length <= 0 && distance <= shootingDistance;
    }

    public void OnDead()
    {
        GetComponent<SpriteRenderer>().sprite = dead[index];
        Instantiate(skull, transform.position + Vector3.up * .2f, Quaternion.identity);
    }
}
