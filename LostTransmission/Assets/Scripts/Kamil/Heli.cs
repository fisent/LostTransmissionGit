﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heli : MonoBehaviour {

    public Transform obj;

    public float t;

    public AnimationCurve position;
    public Vector3 posMin, posDelta;
    public AnimationCurve angle;

    public float hMin, hMax, h;

    public float moveTime, gameTime;

    private IEnumerator Start()
    {
        float lastA = 0;
        while (Time.timeSinceLevelLoad < gameTime)
        {
            transform.localPosition = posMin + posDelta * position.Evaluate(Time.time / t) + Vector3.up * h;
            var a = angle.Evaluate(Time.time / t);
            if(lastA * a < 0)
            {
                h = Random.Range(hMin, hMax);
            }
            lastA = a;
            obj.eulerAngles = new Vector3(0f, 0f, a);
            obj.GetComponent<SpriteRenderer>().flipX = a > 0;
            yield return null;
        }
        var pos = transform.localPosition;
        for(float x = 0f; x < 1f; x += Time.deltaTime / moveTime)
        {
            transform.localPosition = Vector3.Lerp(pos, Vector3.zero, x);
            yield return null;
        }
        WinLose.Ending(true);
    }
}
