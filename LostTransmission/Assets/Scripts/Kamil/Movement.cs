﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour {

    [SerializeField]
    protected Transform head;
    [SerializeField]
    private float speedLimit, acceleration;
    private Rigidbody2D body;
    protected Vector2 motion;

    protected virtual void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        var velocity = body.velocity;
        var targetSpeed = motion.normalized * speedLimit;
        var delta = targetSpeed - velocity;
        if (delta.magnitude > acceleration)
            delta = delta.normalized * acceleration;
        body.AddForce(delta, ForceMode2D.Impulse);
    }

    public void PushBack(float force)
    {
        body.AddForce(-head.up * force, ForceMode2D.Impulse);
    }

    public void PushBackInRange(Vector2 range)
    {
        PushBack(Random.Range(range.x, range.y));
    }
}
