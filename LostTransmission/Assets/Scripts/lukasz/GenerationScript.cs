﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GenerationScript : MonoBehaviour
{
    [SerializeField]
    private GameObject[] tiles;
    [SerializeField]
    private GameObject[] objects;
    [SerializeField]
    private GameObject[] buildings;
    [SerializeField]
    private GameObject[] roads;
    [SerializeField]
    private GameObject[] trees;
    [SerializeField]
    private GameObject[] lakes;
    [SerializeField]
    private GameObject bound;
    [SerializeField]
    private int width;
    [SerializeField]
    private int height;
    [SerializeField]
    private float chanceOfCleanTile;
    [SerializeField]
    private int citySize;
    [SerializeField]
    private int roadLength;

    private bool[,] genMatrix;

    private void PlaceAll()
    {
        PlaceTiles();
        PlaceRoad(width / 2, height / 2);
        PlaceOasis();
        PlaceCity(citySize * 2);
        PlaceCity(citySize * 2);
        PlaceCity(citySize * 2);
        PlaceCity(citySize * 2);
        PlaceCity(citySize * 2);
        for (int i = 0; i < width * height * 1; i++)
            PlaceObject();
        PlaceBounds();
    }

    private void PlaceObject()
    {
        if (GenMatrixFull())
        {
            Debug.LogWarning("Plansza pełna");
            return;
        }

        int x, y;
        bool done = false;
        while (!done)
        {
            x = Random.Range(0, width);
            y = Random.Range(0, height);
            if (!genMatrix[x, y])
            {
                GameObject go = Instantiate(objects[Random.Range(0, objects.Length)]);
                go.transform.position = new Vector3(x + Random.Range(0f, 1f), y + Random.Range(0f, 1f), -1);
                //genMatrix[x, y] = true;
                done = true;
            }
        }
    }

    private void PlaceTree(int oasisBottomLeftX, int oasisBottomLeftY)
    {
        if (GenMatrixFull())
        {
            Debug.LogWarning("Plansza pełna");
            return;
        }

        int x, y;
        bool done = false;
        int tryCounter = 100;
        while (!done && tryCounter > 0)
        {
            x = Random.Range(oasisBottomLeftX, oasisBottomLeftX + 5);
            y = Random.Range(oasisBottomLeftY, oasisBottomLeftY + 5);
            if (oasisBottomLeftX + 5 >= width || oasisBottomLeftY + 5 >= height) return;
            GameObject go = Instantiate(trees[Random.Range(0, trees.Length)]);
            go.transform.position = new Vector3(x + Random.Range(-1f, 1f), y + Random.Range(-1f, 1f), 0);
            genMatrix[x, y] = true;
            done = true;
            tryCounter--;
        }
    }

    private void PlaceTiles()
    {
        for (int i = -10; i < width + 10; i++)
        {
            for (int j = -10; j < height + 10; j++)
            {
                //Tile o indeksie 0 ma ponad 50% szans na pojawienie się
                GameObject tile;
                tile = (Random.value > chanceOfCleanTile) ? tiles[0] : tiles[Random.Range(0, tiles.Length)];
                var go = Instantiate(tile);
                go.transform.position = new Vector3(i, j, 500);
                go.transform.Rotate(0, 0, Random.Range(0, 3) * 90);
            }
        }
    }

    //oaza ma ustaloną wielkość: 5
    private void PlaceOasis()
    {
        int tryCounter = 100;
        int left_bottom_x, left_bottom_y;
        bool done = false;
        while (!done && tryCounter > 0)
        {
            left_bottom_x = Random.Range(0 + 3, width - 3);
            left_bottom_y = Random.Range(0 + 3, height - 3);

            if (!genMatrix[left_bottom_x + 2, left_bottom_x + 2] && !genMatrix[left_bottom_x + 3, left_bottom_y + 2])
            {
                GameObject lake = Instantiate(lakes[Random.Range(0, lakes.Length)]);
                lake.transform.position = new Vector3(left_bottom_x + 2, left_bottom_y + 2, -5);

                MarkPlaceOfBuilding(left_bottom_x, left_bottom_y, 4);

                for (int i = 0; i < 30; i++)
                {
                    PlaceTree(left_bottom_x, left_bottom_y);
                }
                done = true;
            }
            tryCounter--;
        }

    }

    // Założenie: budynek ma 200x200 pikseli
    private void PlaceCity(int numberOfBuildings)
    {
        int cityMargin = citySize / 2;
        int cityCenterX = Random.Range(cityMargin, width - cityMargin);
        int cityCenterY = Random.Range(cityMargin, height - cityMargin);

        for (int i = 0; i < numberOfBuildings; i++)
        {
            bool placed = false;
            int failureCounter = 100;
            while (failureCounter > 0 && !placed)
            {
                int x = Random.Range(cityCenterX - citySize / 2, cityCenterX + citySize / 2);
                int y = Random.Range(cityCenterY - citySize / 2, cityCenterY + citySize / 2);
                if (PlaceForBuildingCheck(x, y, 2))
                {
                    GameObject go = Instantiate(buildings[Random.Range(0, buildings.Length)]);
                    float deltaX = Random.Range(-1f, 1f);
                    float deltaY = Random.Range(-1f, 1f);
                    go.transform.position = new Vector3(x + deltaX, y + deltaY, 0);
                    go.transform.Rotate(1, 0, 0);
                    MarkPlaceOfBuilding(x, y, 2);
                    placed = true;
                }
                failureCounter--;
            }
        }
        //MarkPlaceOfBuilding(cityCenterX - citySize/2, cityCenterY - citySize/2, citySize-1);
    }

    private void PlaceRoad(int x, int y)
    {
        GameObject lastRoad = Instantiate(roads[0]);
        lastRoad.GetComponent<Road>().SetEnterDirection(Road.Directions.DOWN);
        lastRoad.transform.position = new Vector3(x, y, 100);
        for(int i = 0; i < roadLength; i++)
        {
            Road.Directions currentEnter = Road.Flip(lastRoad.GetComponent<Road>().exitDirection);
            GameObject ro = Instantiate(GetRoadWithDirection(Road.Flip(lastRoad.GetComponent<Road>().exitDirection)));
            Road.Directions currentExit = Road.Flip(ro.GetComponent<Road>().GetOtherDirection(currentEnter));
            ro.GetComponent<Road>().SetEnterDirection(currentEnter);
            //if (!(ro.GetComponent<Road>().exitDirection == currentExit)) throw new System.Exception("CHUJCHUJCHUJ");
            Vector3 position = lastRoad.transform.position + Road.directionsDictionary[lastRoad.GetComponent<Road>().exitDirection];
            ro.transform.position = position;
            try
            {
                genMatrix[(int)position.x, (int)position.y] = true;
            }catch(System.Exception e)
            {
                return;
            }
            lastRoad = ro;
        }
    }

    private void PlaceBounds()
    {
        GameObject north, south, east, west;
        north = Instantiate(bound);
        north.GetComponent<BoxCollider2D>().size = new Vector2(width+10, 1);
        north.transform.position = new Vector3(width / 2, height);
        south = Instantiate(bound);
        south.GetComponent<BoxCollider2D>().size = new Vector2(width+10, 1);
        south.transform.position = new Vector3(width / 2, 0);
        east = Instantiate(bound);
        east.GetComponent<BoxCollider2D>().size = new Vector2(1, height+10);
        east.transform.position = new Vector3(width, height/2);
        west = Instantiate(bound);
        west.GetComponent<BoxCollider2D>().size = new Vector2(1, height+10);
        west.transform.position = new Vector3(0, height/2);


    }
    
    void Start()
    {
        genMatrix = new bool[width, height];
        if(width < citySize * 2 || height < citySize * 2)
        {
            Debug.LogError("ERROR: Za duże miasto albo za mała mapa");
        }

        PlaceAll();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("GenerationTest");
        }
    }

    /*private void OnGUI()
    {
        if (GUILayout.Button("Generate background"))
        {
            PlaceTiles();
        }
        if (GUILayout.Button("Generate objects"))
        {
            for (int i = 0; i < 10; i++)
            {
                PlaceObject();
            }
        }
        if(GUILayout.Button("Generate town"))
        {
            PlaceCity(3);
        }
        if(GUILayout.Button("Debug genMatrix"))
        {
            string deb = "";
            int i = 0;
            foreach(var value in genMatrix)
            {
                deb += value ? "1," : "0,";
                if (i % width == width-1) deb += "\n";
                i++;
            }
            Debug.Log(deb);
        }
        if(GUILayout.Button("Generate Road"))
        {
            PlaceRoad(width/2, height/2);
        }
        if (GUILayout.Button("Generate ALL :D"))
        {
            for(int i = 0; i < parent.transform.childCount; i++)
            {
                GameObject.Destroy(parent.transform.GetChild(i).gameObject);
            }
            PlaceAll();
        }
    }*/

    private bool GenMatrixFull()
    {
        bool result = true;
        foreach(bool b in genMatrix)
        {
            result = result && b;
        }
        return result;
    }

    private bool PlaceForBuildingCheck(int x, int y, int size)
    {
        size++;
        if (x + size >= width || y + size >= height) return false;
        var enoughPlace = false;
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                enoughPlace |= genMatrix[x + i, y + j];
        return !enoughPlace;
    }

    private void MarkPlaceOfBuilding(int x, int y, int size)
    {
        if (x + size >= width || y + size >= height) return;
        size++;
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                genMatrix[x + i, y + j] = true;
    }

    private GameObject GetRoadWithDirection(Road.Directions dir)
    {
        List<GameObject> outputList = new List<GameObject>();
        foreach(var road in roads)
        {
            if (dir == Road.Directions.UP && road.GetComponent<Road>().up) outputList.Add(road);
            if (dir == Road.Directions.DOWN && road.GetComponent<Road>().down) outputList.Add(road);
            if (dir == Road.Directions.RIGHT && road.GetComponent<Road>().right) outputList.Add(road);
            if (dir == Road.Directions.LEFT && road.GetComponent<Road>().left) outputList.Add(road);
        }
        return outputList[Random.Range(0, outputList.Count)];
    }

    [SerializeField]
    private GameObject parent;
    private GameObject Instantiate(GameObject go)
    {
        GameObject g = GameObject.Instantiate(go);
        g.transform.parent = parent.transform;
        return g;
    }
}