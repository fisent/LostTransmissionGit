﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour {

    public enum Directions { UP, LEFT, DOWN, RIGHT, ERROR};

    public static Dictionary<Directions, Vector3> directionsDictionary = new Dictionary<Directions, Vector3>() {
        { Directions.UP,new Vector3(0,1)},
        { Directions.DOWN, new Vector3(0,-1)},
        { Directions.LEFT,new Vector3(-1,0)},
        { Directions.RIGHT,new Vector3(1,0)}
    };

    public bool up;
    public bool down;
    public bool left;
    public bool right;

    [SerializeField]
    private Directions enterDirection;
    public Directions exitDirection;

    public void SetEnterDirection(Directions dir)
    {
        enterDirection = dir;
        exitDirection = transform.gameObject.GetComponent<Road>().GetOtherDirection(dir);
    }

    public Directions GetOtherDirection(Directions dir)
    {
        if(dir == Directions.UP)
        {
            if (down) return Directions.DOWN;
            if (right) return Directions.RIGHT;
            if (left) return Directions.LEFT;
        } else if (dir == Directions.DOWN)
        {
            if (up) return Directions.UP;
            if (right) return Directions.RIGHT;
            if (left) return Directions.LEFT;
        } else if (dir == Directions.RIGHT)
        {
            if (up) return Directions.UP;
            if (down) return Directions.DOWN;
            if (left) return Directions.LEFT;
        } else if (dir == Directions.LEFT)
        {
            if (up) return Directions.UP;
            if (right) return Directions.RIGHT;
            if (down) return Directions.DOWN;
        }
        throw new System.Exception("Błąd w definiowaniu kierunków!!!");
        return Directions.ERROR;
    }

    public static Directions Flip(Directions dir)
    {
        switch (dir)
        {
            case Directions.UP: return Directions.DOWN;
            case Directions.DOWN: return Directions.UP;
            case Directions.LEFT: return Directions.RIGHT;
            case Directions.RIGHT: return Directions.LEFT;
        }
        return Directions.ERROR;
    }
}
