﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LanguageChooseMenuScript : MonoBehaviour {

	public void English()
    {
        SceneManager.LoadScene("intro_US");
    }

    public void Polish()
    {
        SceneManager.LoadScene("intro_PL");
    }

    public void Russian()
    {
        SceneManager.LoadScene("intro_RU");
    }

    public void Martian()
    {
        SceneManager.LoadScene("intro_MA");
    }
}
