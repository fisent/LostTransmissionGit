﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinLose : MonoBehaviour {

    public GameObject[] winnObjects, loseObjects;

    public void Awake()
    {
        foreach (GameObject go in winnObjects)
            go.SetActive(false);
        foreach (GameObject go in loseObjects)
            go.SetActive(false);
    }

    public static void Ending(bool win)
    {
        FindObjectOfType<WinLose>().End(win);
    }

    public void End(bool winn)
    {
        foreach (GameObject go in (winn ? winnObjects : loseObjects))
            go.SetActive(true);
    }
}
